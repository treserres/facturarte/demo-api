import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearEmpresaComponent } from './empresas/crear/crear.component';
import { ConsultarEmpresaComponent } from './empresas/consultar/consultar.component';
import { BorrarEmpresaComponent } from './empresas/borrar/borrar.component';
import { BorrarEnteComponent } from './empresas/borrar-ente/borrar-ente.component';
import { AsociarEmpresaComponent } from './empresas/asociar/asociar.component';
import { DatosFacturacionComponent } from './empresas/datosf/datosf.component';
import { DesignarRepresentanteComponent } from './empresas/designar/designar.component';
import { PaginaComponent } from './pagina/pagina.component';
import { PingComponent } from './ping/ping.component';
import { PedirCertificadoComponent } from './afip/pedir-certificado/pedir-certificado.component';
import { EstadoComponent } from './afip/estado/estado.component';
import { SoaComponent } from './soa/soa.component';
import { DgrComponent } from './dgr/dgr.component';
import { EmitirFacturaComponent } from './facturacion/emitir/emitir.component';
import { ConsultarFacturacionComponent } from './facturacion/consultar/consultar.component';
import { AutocompleteFacturaComponent } from './facturacion/autocomplete/autocomplete.component';

const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: '/consideraciones' },
	{ path: 'ping', component: PingComponent },
	{ path: 'soa', component: SoaComponent },
	{ path: 'dgr', component: DgrComponent },
	{
		path: 'empresa',
		children: [
			{ path: 'crear', component: CrearEmpresaComponent },
			{ path: 'consultar', component: ConsultarEmpresaComponent },
			{ path: 'borrar', component: BorrarEmpresaComponent },
			{ path: 'asociar', component: AsociarEmpresaComponent },
			{ path: 'desasociar', component: BorrarEnteComponent },
			{ path: 'representante', component: DesignarRepresentanteComponent },
			{ path: 'datosfacturacion', component: DatosFacturacionComponent}
		]
	},
	{
		path: 'afip',
		children: [
			{ path: 'csr', component: PedirCertificadoComponent },
			{ path: 'estado', component: EstadoComponent }
		]
	},
	{
		path: 'facturacion',
		children: [
			{ path: 'emitir', component: EmitirFacturaComponent},
			{ path: 'consultar', component: ConsultarFacturacionComponent},
			{ path: 'autocomplete', component: AutocompleteFacturaComponent },
		]
	},
	{ path: ':pagina', component: PaginaComponent },
	{ path: '**', redirectTo: '/' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
