import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-pagina',
	templateUrl: './pagina.component.html',
	styleUrls: ['./pagina.component.scss']
})
export class PaginaComponent implements OnInit, OnDestroy {

	constructor(
		private route: ActivatedRoute
	) { }

	sub;
	pagina;

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			switch (params['pagina']) {
				case 'codigoserror':
					this.pagina = 'codigos_error';
					break;
				case 'codigosrespuesta':
					this.pagina = 'codigos_respuesta';
					break;
				default:
					this.pagina = params['pagina'];
					break;
			}
		});
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

}
