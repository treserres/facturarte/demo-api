import { Component, ViewChild, ElementRef } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-designar-empresa',
	templateUrl: './designar.component.html',
	styleUrls: ['./designar.component.scss']
})
export class DesignarRepresentanteComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) { }

	cuitRepresentante = '';

	@ViewChild('fieldCuitRepresentante') fieldCuitRepresentante: ElementRef;

	url = '/empresa/representante';

	onDesignar() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const request = {
			req: this.httpService.post({
					ente: ente,
					cuit: cuit,
					cuit_representante: this.cuitRepresentante
				}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}&cuit_representante=${this.cuitRepresentante}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter(event) {
		if (event.target.name === 'cuit') {
			this.fieldCuitRepresentante.nativeElement.focus();
		} else {
			this.onDesignar();
		}
	}

}
