import { Component, ViewChild } from '@angular/core';
import { UtilService, HttpService } from '@app/core';
import { InputBSComponent } from '@componentes/.';
import { MatBottomSheet } from '@angular/material';

@Component({
	selector: 'app-asociar-empresa',
	templateUrl: './asociar.component.html',
	styleUrls: ['./asociar.component.scss']
})
export class AsociarEmpresaComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService,
		private bottomSheet: MatBottomSheet
	) { }

	certificadoCliente = null;
	url = '/empresa/asociar';
	estado = 'pendiente';

	@ViewChild('archivo') archivo;

	onAsociar() {
		if (this.certificadoCliente) {
			const ente = this.utilService.ente;
			const cuit = this.utilService.cuit;
			const request = {
				req: this.httpService.post({
						ente: ente,
						cuit: cuit,
						crt: this.certificadoCliente,
					}, this.url),
				curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}&crt=${encodeURIComponent(this.certificadoCliente)}" ${this.utilService.getBaseUrl()}${this.url}`
			};
			this.utilService.onServerRequest.next(request);
		} else if (this.estado === 'pendiente') {
			this.utilService.toast('Debe cargar el certificado de AFIP');
		} else if (this.estado === 'error') {
			this.utilService.toast('El certificado de AFIP cargado no es correcto');
		}
	}

	onEnter() {
		this.onAsociar();
	}

	cargaArchivo(event) {
		this.utilService.leeArchivo(event.target, 'text', () => this.reset(true)).then((result: any) => {
			this.validarCertificado(result.toString());
		});
	}

	onCargaManual() {
		const bs = this.bottomSheet.open(InputBSComponent, {
			data: {
				tipo: 'textarea',
				placeholder: 'Certificado'
			}
		});
		bs.afterDismissed().subscribe((respuesta: { tipo: string, value: string }) => {
			this.validarCertificado(respuesta.value);
		});
	}

	validarCertificado(certificado: string) {
		if (certificado) {
			if (certificado.indexOf('-----BEGIN CERTIFICATE-----') !== -1 && certificado.indexOf('-----END CERTIFICATE-----') !== -1) {
				this.certificadoCliente = certificado;
				this.estado = 'ok';
			} else {
				this.reset(true);
			}
		} else {
			this.utilService.toast('No se ingresó certificado');
		}
	}

	clickInput(archivo) {
		if (this.certificadoCliente === null) {
			archivo.click();
		} else {
			this.reset(false);
		}
	}

	reset(error?: boolean) {
		this.certificadoCliente = null;
		if (error) {
			this.estado = 'error';
			this.utilService.toast('El certificado de AFIP cargado no es correcto');
		} else {
			this.estado = 'pendiente';
		}
		this.archivo.nativeElement.value = '';
	}

}
