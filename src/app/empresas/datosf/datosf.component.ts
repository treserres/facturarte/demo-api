import { Component, ViewChild, AfterViewInit, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { UtilService, HttpService, FocusService } from '@app/core';
import { FechaComponent } from '@componentes/fecha';
import { MatSelect, MatTabChangeEvent } from '@angular/material';
import { FocusNextDirective } from '@app/shared';

@Component({
	selector: 'app-datosf-empresa',
	templateUrl: './datosf.component.html',
	styleUrls: ['./datosf.component.scss']
})
export class DatosFacturacionComponent implements AfterViewInit {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService,
		private focusService: FocusService
	) {
		this.utilService.onEnter.subscribe((e) => this.onEnter(e));
	}

	url = '/empresa/facturacion';

	puestodefecto = '';
	razSocial = '';
	nombreFantasia = '';
	domicilio = '';
	localidad = '';
	provincia = '';
	codPostal = '';
	telefono = '';
	inicioActividad = '';
	ingresosBrutos = '';
	email = '';
	ptoVenta = '';

	hayBusqueda = false;

	logo = null;

	@ViewChild(FechaComponent) fechaInput: FechaComponent;

	@ViewChild('logoInput') logoInput: ElementRef;

	datosf: any = {
		talonarios: [],
		dtsEmpresa: {},
		formato: 0,
		id: ''
	};

	puntosventa = [];

	tab = 0;

	@ViewChild('fieldPuntoventa') fieldPuntoventa: MatSelect;

	@ViewChildren(FocusNextDirective) focusElements: QueryList<FocusNextDirective>;

	ngAfterViewInit() {
		this.fieldPuntoventa.selectionChange.subscribe((ev) => {
			if (ev.value) {
				this.onSet();
			}
		});
	}

	onConsultar() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const request = {
			req: this.httpService.get({
				ente: ente,
				cuit: cuit
			}, this.url),
			name: 'traeDatos',
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" "${this.utilService.getBaseUrl()}${this.url}?ente=${ente}&cuit=${cuit}"`
		};
		this.utilService.onServerRequest.next(request);
	}

	onRespuesta(respuesta) {
		this.datosf = respuesta;
		this.puntosventa = [];
		this.datosf.talonarios.forEach((talonario) => {
			if (this.puntosventa.indexOf(talonario.puesto) === -1) {
				this.puntosventa.push(talonario.puesto);
			}
		});
		this.setFormulario();
	}

	setFormulario() {
		this.razSocial = this.datosf.dtsEmpresa.razSocial;
		this.nombreFantasia = this.datosf.dtsEmpresa.nombreFantasia;
		this.domicilio = this.datosf.dtsEmpresa.domicilio;
		this.localidad = this.datosf.dtsEmpresa.localidad;
		this.provincia = this.datosf.dtsEmpresa.provincia;
		this.codPostal = this.datosf.dtsEmpresa.codPostal;
		this.telefono = this.datosf.dtsEmpresa.telefono;
		this.inicioActividad = this.datosf.dtsEmpresa.inicioActividad;
		this.ingresosBrutos = this.datosf.dtsEmpresa.ingresosBrutos;
		this.email = this.datosf.dtsEmpresa.email;
		const defecto = this.datosf.talonarios.find((talonario) => {
			return talonario.defecto;
		});
		if (defecto) {
			this.ptoVenta = defecto.puesto;
		} else {
			this.ptoVenta = '';
		}
		this.logo = this.datosf.dtsEmpresa.logo;
		this.hayBusqueda = true;
	}

	onEnter(event) {
		if (this.tab === 1) {
			this.focusElements.first.focus();
		} else {
			this.focusService.nextFocus(1, this.focusElements);
		}
	}

	onCambiaTab(event: MatTabChangeEvent) {
		this.utilService.onResetRespuesta.next();
		this.tab = event.index;
		switch (event.index) {
			case 0: // Documentación
				this.utilService.onUrlChange.emit(null);
				break;
			case 1: // Consultar
				this.utilService.onUrlChange.emit({
					method: 'get',
					url: this.url
				});
				break;
			case 2: // Establecer
				this.utilService.onUrlChange.emit({
					method: 'post | put | patch',
					url: this.url
				});
				break;
		}
	}

	onSet() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const datos: any = {
			ente: ente,
			cuit: cuit,
			razSocial: this.razSocial,
			nombreFantasia: this.nombreFantasia,
			domicilio: this.domicilio,
			localidad: this.localidad,
			provincia: this.provincia,
			codPostal: this.codPostal,
			telefono: this.telefono,
			inicioActividad: this.inicioActividad,
			ingresosBrutos: this.ingresosBrutos,
			email: this.email,
			ptoVenta: this.ptoVenta
		};
		if (this.logo !== null) {
			datos.logo = this.logo;
		}
		const request = {
			req: this.httpService.post(datos, this.url),
			name: 'EstableceDatos',
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}&razSocial=${encodeURIComponent(this.razSocial)}&razSocial=${encodeURIComponent(this.razSocial)}&nombreFantasia=${encodeURIComponent(this.nombreFantasia)}&domicilio=${encodeURIComponent(this.domicilio)}&localidad=${encodeURIComponent(this.localidad)}&provincia=${encodeURIComponent(this.provincia)}&codPostal=${encodeURIComponent(this.codPostal)}&telefono=${encodeURIComponent(this.telefono)}&inicioActividad=${encodeURIComponent(this.inicioActividad)}&ingresosBrutos=${encodeURIComponent(this.ingresosBrutos)}&email=${encodeURIComponent(this.email)}&ptoVenta=${this.ptoVenta}${datos.logo !== null ? '&logo=' + encodeURIComponent(this.logo) : ''}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onCambiaLogo(ev) {
		this.utilService.leeArchivo(ev.target, 'dataurl', () => {
			this.utilService.toast('Error al cargar la imagen');
		}).then(result => {
			this.logo = result;
		});
	}

	quitarLogo() {
		this.logoInput.nativeElement.value = '';
		this.logo = '';
	}

	onClickTabs(ev: any) {
		if (ev.target.innerHTML.includes('Establecer') && !this.hayBusqueda) {
			this.utilService.toast('Primero debe ejecutar "Consultar"');
		}
	}

}
