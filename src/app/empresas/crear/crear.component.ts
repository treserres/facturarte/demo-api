import { Component } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-crear-empresa',
	templateUrl: './crear.component.html',
	styleUrls: ['./crear.component.scss']
})
export class CrearEmpresaComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) {
		this.utilService.onEnter.subscribe(() => this.onEnter());
	}

	url = '/empresa/nueva';

	onCrear() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const request = {
			req: this.httpService.post({
					ente: ente,
					cuit: cuit,
				}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter() {
		this.onCrear();
	}

}
