import { Component } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-borrar-ente-empresa',
	templateUrl: './borrar-ente.component.html',
	styleUrls: ['./borrar-ente.component.scss']
})
export class BorrarEnteComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) { }

	url = '/empresa/borrar';

	onBorrar() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const request = {
			req: this.httpService.delete({
					ente: ente,
					cuit: cuit,
				}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter() {
		this.onBorrar();
	}

}
