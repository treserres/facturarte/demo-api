import { Injectable, EventEmitter, QueryList } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { environment } from '@env';
import { AppComponent } from '@app/app.component';
import * as dayjs from 'dayjs';
import { FormGroup } from '@angular/forms';

@Injectable()
export class UtilService {

	constructor(
		private snackBar: MatSnackBar
	) { }

	onServerRequest: EventEmitter<any> = new EventEmitter<any>();
	onResetRespuesta: EventEmitter<any> = new EventEmitter<any>();
	onEnter: EventEmitter<any> = new EventEmitter<any>();
	onUrlChange: EventEmitter<any> = new EventEmitter<any>();

	ente = '';
	cuit = '';

	focusShift = false;
	onFocusNext: EventEmitter<number> = new EventEmitter<number>();

	toast(message, duration?) {
		this.snackBar.open(message, 'Cerrar', {
			duration: duration ? duration : 5000,
			horizontalPosition: 'end',
			verticalPosition: 'top'
		});
	}

	getAuth() {
		return environment.auth;
	}

	getBaseUrl() {
		return environment.baseUrl;
	}

	validarCuit(cuit?) {
		if (cuit === undefined) {
			cuit = this.cuit;
		}
		if (cuit.length !== 11) {
			return false;
		}
		let acumulado = 0;
		const digitos = cuit.split('');
		const digito = Number.parseInt(digitos.pop(), 10);
		for (let i = 0; i < digitos.length; i++) {
			acumulado += digitos[9 - i] * (2 + (i % 6));
		}
		let verif = 11 - (acumulado % 11);
		if (verif === 11) {
			verif = 0;
		}
		return digito === verif;
	}

	leeArchivo(input, readAs, onerror: Function) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = () => {
				resolve(reader.result);
			};
			reader.onerror = () => {
				onerror();
				reject();
			};
			switch (readAs) {
				case 'text':
					reader.readAsText(input.files[0]);
					break;
				case 'dataurl':
					reader.readAsDataURL(input.files[0]);
					break;
			}
		});
	}

}
