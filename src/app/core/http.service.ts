import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { environment } from '@env';
import { MatSnackBar } from '@angular/material/snack-bar';

const urlOptions: any = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'Authentication': environment.auth
	}),
	observe: 'events',
	withCredentials: true
};

const commonUrlOptions: any = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'Authentication': environment.auth
	}),
	withCredentials: true
};

const urlOptionsBlobGet: any = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'Authentication': environment.auth
	}),
	observe: 'response',
	reportProgress: false,
	params: new HttpParams(),
	responseType: 'blob',
	withCredentials: true
};

const urlOptionsBlobPost: any = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'Authentication': environment.auth
	}),
	observe: 'body',
	reportProgress: false,
	params: new HttpParams(),
	responseType: 'blob',
	withCredentials: true
};

@Injectable()
export class HttpService {

	constructor(
		private http: HttpClient,
		private snackBar: MatSnackBar
	) { }

	private baseUrl = environment.baseUrl;

	get(datos: any = null, url?: string, blob?: boolean) {
		return new Promise(resolve => {
			const options = this.paramsRequest(datos, blob);
			this.http.get<any>(this.baseUrl + url, options)
				.subscribe((res: HttpResponse<any>) => this.procesa(res, resolve), err => this.error(err, resolve));
		});
	}

	async getAuto(url: string, datos: any = null): Promise<any> {
		const options = datos ? this.paramsRequest(datos, false, true) : commonUrlOptions;
		try {
			const res = await this.http.get<any>(this.baseUrl + url, options).toPromise();
			return res;
		} catch (error) {
			this.errorAuto(error);
			return null;
		}
	}

	private errorAuto(err) {
		console.error(err);
		this.snackBar.open('Ha ocurrido un error', null, {
			duration: 5000,
			horizontalPosition: 'end',
			verticalPosition: 'top'
		});
		throw new Error('Ocurrió un error en una llamada al backend');
	}

	/**
	 * Trae los datos pedidos filtrados por coincidencia con el texto provisto
	 * @param que - Qué busca
	 * @param texto - Texto a buscar
	 * @param datos (opcional) - Parámetros adicinoales
	 * @example autocomplete('rubros','activo')
	 * @return Array con resultados
	 */
	async autocomplete(que: string, texto: string, datos?: any) {
		if (!datos) {
			datos = {};
		}
		return await this.getAuto(`/facturacion/autocomplete/${que}`, Object.assign({ texto: texto }, datos));
	}

	delete(datos: any, url?: string, blob?: boolean) {
		return new Promise(resolve => {
			const options = this.paramsRequest(datos, blob);
			this.http.delete<any>(this.baseUrl + url, options)
				.subscribe((res: HttpResponse<any>) => this.procesa(res, resolve), err => this.error(err, resolve));
		});
	}

	post(datos: any, url?: string, blob?: boolean) {
		return new Promise(resolve => {
			this.http.post<any>(this.baseUrl + url, datos, blob ? urlOptionsBlobPost : urlOptions)
				.subscribe((res: HttpResponse<any>) => this.procesa(res, resolve), err => this.error(err, resolve));
		});
	}

	file(formData: FormData, url: string) {
		return new Promise(resolve => {
			const options = urlOptions;
			options.headers = new HttpHeaders({
				'Authentication': environment.auth
			});
			this.http.post(this.baseUrl + url, formData)
				.subscribe((res: HttpResponse<any>) => this.procesa(res, resolve), err => this.error(err, resolve));
		});
	}

	private procesa(res: HttpResponse<any>, resolve) {
		if (res instanceof Blob) {
			resolve(res);
		}
		if (res.headers) {
			if (res) {
				resolve(this.formatearRespuesta(res));
			} else {
				resolve(null);
			}
		}
	}

	private error(error, resolve) {
		error.body = error.error;
		resolve(this.formatearRespuesta(error));
	}

	private formatearRespuesta(res: HttpResponse<any>) {
		const respuesta: any = {};
		respuesta.body = res.body;
		respuesta.headers = `
			<span class="status-code _${res.status.toString().substring(0, 1)}00">${res.status} ${res.statusText}</span><br><br>
			<span class="url">${res.url}</span><br><br>
			`;
		res.headers.keys().forEach(key => {
			respuesta.headers += `<b>${key}</b>: ${res.headers.get(key)}<br>
			`;
		});
		return respuesta;
	}

	private paramsRequest(datos: any, blob: boolean, common: boolean = false) {
		let params = new HttpParams();
		for (const key in datos) {
			if (datos.hasOwnProperty(key)) {
				params = params.set(key, datos[key]);
			}
		}
		return Object.assign({ params: params }, common ? commonUrlOptions : (blob ? urlOptionsBlobGet : urlOptions));
	}

}

