import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UtilService } from '@app/core';
import { InputBSComponent } from '@componentes/inputbs';
import { MatBottomSheet } from '@angular/material';

@Component({
	selector: 'app-entecuit',
	templateUrl: './entecuit.component.html',
	styleUrls: ['./entecuit.component.scss']
})
export class EntecuitComponent implements OnInit {

	constructor(
		public utilService: UtilService,
		private bottomSheet: MatBottomSheet
	) { }

	@ViewChild('cuitField') cuitField: ElementRef;

	entes = [];

	entesDefecto = [
		'Usuario1',
		'Usuario2',
		'Usuario3',
		'Usuario4'
	];


	ngOnInit() {
		const ente = localStorage.getItem('ente');
		const cuit = localStorage.getItem('cuit');
		if (cuit && cuit.length > 0) {
			this.utilService.cuit = cuit;
		}
		let entes = JSON.parse(localStorage.getItem('entes'));
		if (entes === null) {
			entes = [];
			localStorage.setItem('entes', JSON.stringify(entes));
		} else {
			entes.forEach((enteItem) => {
				this.entes.unshift(enteItem);
			});
		}
		this.entesDefecto.forEach((enteItem) => {
			this.entes.push(enteItem);
		});
		if (ente && ente.length > 0 && this.entes.indexOf(ente) !== -1) {
			this.utilService.ente = ente;
		}
	}

	esEnteDefecto() {
		return this.utilService.ente.length === 0 || this.entesDefecto.indexOf(this.utilService.ente) !== -1;
	}

	agregarEnte() {

		this.bottomSheet.open(InputBSComponent, {
			data: { tipo: 'input', placeholder: 'Nombre del Ente' }
		}).afterDismissed().subscribe((ente) => {
			if (ente && ente.value.length > 0) {
				const entes = JSON.parse(localStorage.getItem('entes'));
				this.entes.unshift(ente.value);
				entes.unshift(ente.value);
				localStorage.setItem('entes', JSON.stringify(entes));
				this.utilService.ente = ente.value;
			}
		});
	}

	eliminarEnte() {
		if (this.entes.length > 1) {
			let entes = JSON.parse(localStorage.getItem('entes'));
			if (entes) {
				entes = entes.filter((ente) => ente !== this.utilService.ente);
				localStorage.setItem('entes', JSON.stringify(entes));
			}
			this.entes = this.entes.filter((ente) => ente !== this.utilService.ente);
			this.utilService.ente = '';
		}
	}

	onSelect(open: boolean) {
		if (!open) {
			setTimeout(() => {
				this.cuitField.nativeElement.focus();
			}, 100);
		}
	}

	onEnter(ev) {
		this.utilService.onEnter.emit(ev);
	}

}
