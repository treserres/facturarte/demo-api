import { Component, ViewChild, ElementRef } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-estado',
	templateUrl: './estado.component.html',
	styleUrls: ['./estado.component.scss']
})
export class EstadoComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) { }

	url = '/afip/estado/{cuit}';
	cuitEstado = '';
	@ViewChild('fieldCuit') fieldCuit: ElementRef;

	onVerificar() {
		if (this.utilService.validarCuit(this.cuitEstado)) {
			const request = {
				req: this.httpService.get({}, this.url.replace('{cuit}', this.cuitEstado)),
				curl: `curl --header "Authentication:${this.utilService.getAuth()}" "${this.utilService.getBaseUrl()}${this.url.replace('{cuit}', this.cuitEstado)}"`
			};
			this.utilService.onServerRequest.next(request);
		} else {
			this.utilService.toast('CUIT no válido');
		}
	}

	onEnter(event) {
		if (event.target.name === 'cuit') {
			this.fieldCuit.nativeElement.focus();
		} else {
			this.onVerificar();
		}
	}

}
