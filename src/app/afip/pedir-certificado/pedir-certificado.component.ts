import { Component } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-pedir-certificado',
	templateUrl: './pedir-certificado.component.html',
	styleUrls: ['./pedir-certificado.component.scss']
})
export class PedirCertificadoComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) { }

	url = '/afip/csr';

	onPedir() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const request = {
			req: this.httpService.get({
					ente: ente,
					cuit: cuit,
			}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" "${this.utilService.getBaseUrl()}${this.url}?ente=${ente}&cuit=${cuit}"`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter() {
		this.onPedir();
	}


}
