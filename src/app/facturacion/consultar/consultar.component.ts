import { Component, QueryList, ViewChildren } from '@angular/core';
import { UtilService, HttpService, FocusService } from '@app/core';
import * as dayjs from 'dayjs';
import { FocusNextDirective } from '@app/shared';

@Component({
	selector: 'app-consultar-facturacion',
	templateUrl: './consultar.component.html',
	styleUrls: ['./consultar.component.scss']
})
export class ConsultarFacturacionComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService,
		private focusService: FocusService
	) {
		this.fechaDesde = dayjs().date(1).format('DD/MM/YYYY');
		this.fechaHasta = dayjs().date(dayjs().daysInMonth()).format('DD/MM/YYYY');
	}

	url = '/facturacion/consultar';

	formato = 0;
	limitar = false;
	fechaDesde = '';
	fechaHasta = '';
	pagina = '';
	busqueda = '';

	@ViewChildren(FocusNextDirective) focusElements: QueryList<FocusNextDirective>;

	onSolicitar() {
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const datos: any = {
			ente: ente,
			cuit: cuit,
			desde: this.fechaDesde,
			hasta: this.fechaHasta,
			limitar: this.limitar ? 1 : 0
		};
		let params = `ente=${ente}&cuit=${cuit}&desde=${this.fechaDesde}&hasta=${this.fechaHasta}&limitar=${this.limitar ? 1 : 0}`;
		if (this.formato === 0) { // JSON
			datos.pagina = this.pagina;
			datos.busqueda = this.busqueda;
			params += `&pagina=${this.pagina}&busqueda=${this.busqueda}`;
		} else {
			datos.formato = this.formato;
		}
		const request = {
			req: this.httpService.post(datos, this.url + (this.formato > 0 ? `/${this.formato}` : ''), this.formato !== 0),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "${params}" ${this.utilService.getBaseUrl()}${this.url + (this.formato > 0 ? `/${this.formato}` : '')}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter() {
		this.focusService.nextFocus(0, this.focusElements);
	}
}
