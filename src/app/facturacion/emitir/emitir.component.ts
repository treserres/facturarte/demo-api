import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { UtilService, HttpService } from '@app/core';
import { MatTable, MatSelect } from '@angular/material';
import { FocusNextDirective } from '@app/shared';

@Component({
	selector: 'app-emitir-facturacion',
	templateUrl: './emitir.component.html',
	styleUrls: ['./emitir.component.scss']
})
export class EmitirFacturaComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) {
		this.utilService.onEnter.subscribe((ev) => this.onEnter(ev));
	}

	url = '/facturacion/emitir';

	autocomplete = { cuit: '', queid: 0};

	cuit = '';
	denominacion = '';
	dni = '';
	detalle = '';
	cantidad = '';
	precio = '';
	iva = '0';
	remito = '';
	talonario = '';
	icono = 'person';
	enviarmail:boolean = false;
	diremail = '';

	@ViewChild('fieldIva') fieldIva: MatSelect;

	@ViewChildren(FocusNextDirective) focusElements: QueryList<FocusNextDirective>;

	@ViewChild(MatTable) table: MatTable<any>;
	articulos = [];
	displayedColumns = ['detalle', 'cantidad', 'precio', 'iva', 'acciones'];

	soa(ev: KeyboardEvent | MouseEvent) {
		this.cuit = this.autocomplete.cuit.substr(0,11);
		if (this.utilService.validarCuit(this.cuit.substr(0,11)) && this.cuit.trim().substr(0,11) !== this.utilService.cuit.trim()) {
			this.icono = 'search';
			this.httpService.get({}, '/soa/' + this.cuit).then((res: any) => {
				if (res.body && res.body.success) {
					this.denominacion = res.body.data.nombre;
					// console.log(this.focusElements.find(el => el.name === 'dni').host.disabled);
					this.focusElements.find(el => el.name === 'dni').host.disabled = true;
					this.focusElements.find(el => el.name === 'talonario').focus();
				}
				this.icono = 'person';
			});
		} else {
			this.utilService.toast('CUIT no válido' + this.cuit.substr(0,11));
			setTimeout(() => { this.focusElements.find((el => el.name === 'cuit')).focus(); }, 100);
		}
	}
	traeCuit(evento) {
		if (this.autocomplete.cuit) {
			this.cuit = this.autocomplete.queid.toString();
			this.soa(evento);
			// this.denominacion = this.autocomplete.cuit;
		}
	}

	onEmitirFactura(tipo) {
		const tipofac = (tipo == 1) ? "F" : "C";
		const ente = this.utilService.ente;
		const cuit = this.utilService.cuit;
		const articulos = JSON.stringify(this.articulos);
		const request = {
			req: this.httpService.post({
				ente: ente,
				cuit: cuit,
				cuitFactura: this.cuit,
				denominacionFactura: this.denominacion,
				dniFactura: this.dni,
				remitoFactura: this.remito,
				talonarioFactura: this.talonario,
				tipoFactura: tipofac,
				articulos: articulos,
				enviarMail: this.enviarmail,
				mailFactura: this.diremail

			}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" -d "ente=${ente}&cuit=${cuit}&cuitFactura=${this.cuit}&denominacionFactura=${this.denominacion}&dniFactura=${this.dni}&tipoFactura=${tipofac}&remitoFactura=${this.remito}&talonarioFactura=${this.talonario}&enviarMail=${this.enviarmail}&mailFactura=${this.diremail}&articulos=${articulos}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter(event) {
		this.focusElements.first.focus();
	}

	reset(solotabla) {
		if (!solotabla) {
			this.cuit = '';
			this.denominacion = '';
			this.dni = '';
			this.remito = '';
			this.talonario = '';
			this.focusElements.find(el => el.name === 'dni').host.nativeElement.disabled = false;
			this.articulos = [];
			this.table.renderRows();
		}
		this.detalle = '';
		this.cantidad = '';
		this.precio = '';
	}

	agregarArticulo() {
		if (this.detalle.length > 0 && this.cantidad.length > 0 && this.precio.length > 0 && this.iva.length > 0) {
			this.articulos.push({
				detalle: this.detalle.trim(),
				cantidad: this.cantidad.trim(),
				precio: this.precio.trim(),
				iva: this.iva.trim()
			});
			this.reset(true);
			setTimeout(() => {
				this.focusElements.find(el => el.name === 'detalle').focus();
			}, 100);
			this.table.renderRows();
		} else {
			this.utilService.toast('Debe completar todos los campos del artículo');
		}
	}

	total() {
		let suma = 0;
		this.articulos.forEach((articulo) => {
			const precio = Number.parseFloat(articulo.precio);
			const iva = Number.parseFloat(articulo.iva);
			const cantidad = Number.parseFloat(articulo.cantidad);
			suma += (precio + (precio * iva / 100)) * cantidad;
		});
		return suma;
	}

	editar(articulo) {
		this.articulos = this.articulos.filter((art => art !== articulo));
		this.detalle = articulo.detalle;
		this.cantidad = articulo.cantidad;
		this.precio = articulo.precio;
		this.iva = articulo.iva;
	}

	dtsBuscar() {
		const parametros = { ente: this.utilService.ente, cuit: this.utilService.cuit};
		return parametros;
	}

	eliminar(articulo) {
		this.articulos = this.articulos.filter((art => art !== articulo));
	}

}
