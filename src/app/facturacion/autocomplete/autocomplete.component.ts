import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { UtilService, HttpService } from '@app/core';
import { FocusNextDirective } from '@app/shared';

@Component({
	selector: 'app-autocomplete-facturacion',
	templateUrl: './autocomplete.component.html',
	styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteFacturaComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) {
		this.utilService.onEnter.subscribe((ev) => this.onEnter(ev));
	}

	url = '/facturacion/autocomplete/buscuit';

	autocomplete = { cuit: '', queid: 0};

	queid = 0;
	cuit = '';
	denominacion = '';
	dni = '';
	detalle = '';
	cantidad = '';
	precio = '';
	iva = '0';
	remito = '';
	talonario = '';
	icono = 'person';


	@ViewChildren(FocusNextDirective) focusElements: QueryList<FocusNextDirective>;

	onEnter(event) {
		this.focusElements.first.focus();
	}

	traeCuit(evento) {
		if (this.autocomplete.cuit) {
			this.cuit = this.autocomplete.queid.toString();
			const ente = this.utilService.ente;
			const cuit = this.utilService.cuit;
			const request = {
				req: this.httpService.get({
					ente: ente,
					cuit: cuit,
					texto: this.cuit
				}, this.url),
				curl: `curl --header "Authentication:${this.utilService.getAuth()}" "${this.utilService.getBaseUrl()}${this.url}?texto=${this.cuit}&ente=${ente}&cuit=${cuit}"`
			};
			this.utilService.onServerRequest.next(request);
			// this.soa(evento);
			// this.denominacion = this.autocomplete.cuit;
		}
	}


	dtsBuscar() {
		const parametros = { ente: this.utilService.ente, cuit: this.utilService.cuit };
		return parametros;
	}

}
