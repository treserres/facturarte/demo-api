import { Component, OnInit, AfterViewChecked} from '@angular/core';
import { UtilService } from '@app/core';
import { collapsible } from '@app/shared';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [ collapsible ]
})
export class AppComponent implements OnInit, AfterViewChecked {

	constructor(
		public utilService: UtilService,
	) { }

	secciones: any = [
		{ url: '/consideraciones', titulo: 'Consideraciones' },
		{ url: '/autenticacion', titulo: 'Autenticación' },
		{ url: '/codigosrespuesta', titulo: 'Respuestas' },
		{ url: '/codigoserror', titulo: 'Códigos de error' },
		{ url: '/ping', titulo: 'Ping' },
		{ url: '/soa', titulo: 'SOA' },
		{ url: '/dgr', titulo: 'DGR' },
		{
			titulo: 'Empresa',
			paginas: [
				{ url: '/empresa/crear', titulo: 'Crear' },
				{ url: '/empresa/consultar', titulo: 'Consultar' },
				{ url: '/empresa/borrar', titulo: 'Borrar' },
				{ url: '/empresa/asociar', titulo: 'Asociar ente' },
				{ url: '/empresa/desasociar', titulo: 'Desasociar ente' },
				{ url: '/empresa/datosfacturacion', titulo: 'Datos de facturación' },
				{ url: '/empresa/representante', titulo: 'Designar representante' },
			]
		},
		{
			titulo: 'AFIP',
			paginas: [
				{ url: '/afip/csr', titulo: 'Pedir certificado (CSR)' },
				{ url: '/afip/estado', titulo: 'Chequear estado' },
			]
		},
		{
			titulo: 'Facturación',
			paginas: [
				{ url: '/facturacion/emitir', titulo: 'Emitir' },
				{ url: '/facturacion/consultar', titulo: 'Consultar' },
				{ url: '/facturacion/autocomplete', titulo: 'Autocomplete' },
			]
		},
	];

	component: any;
	apiUrl = null;

	ngOnInit() {
		this.utilService.onEnter.subscribe((ev) => this.component.onEnter(ev));
	}

	ngAfterViewChecked() {
		this.utilService.onUrlChange.subscribe(apiUrl => this.apiUrl = apiUrl);
	}

	onActivate(event) {
		this.component = event;
		this.utilService.onResetRespuesta.next();
	}

	isActive(pagina: string | { url: string, titulo: string, activo?: boolean }) {
		if (typeof pagina === 'string') {
			return window.location.pathname.includes(pagina);
		} else {
			if (window.location.pathname.includes(pagina.url)) {
				/*this.secciones.forEach((seccion) => {
					if (seccion.paginas) {
						seccion.paginas.forEach((pag) => {
							if (pagina === pag) {
								seccion.expandida = true;
							}
						});
					}
				});*/
				return true;
			} else {
				return false;
			}
		}
	}
}
