import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import { UtilService } from '@app/core';

@Component({
	selector: 'app-url',
	templateUrl: './url.component.html',
	styleUrls: ['./url.component.scss']
})
export class UrlComponent implements OnInit, OnDestroy {

	constructor(
		private clipboardService: ClipboardService,
		private utilService: UtilService
	) { }

	@Input() url;
	@Input() method;
	@Input() style = '';
	@Input() emitir = true;

	ngOnInit() {
		setTimeout(() => {
			if (this.emitir) {
				this.utilService.onUrlChange.emit({
					method: this.method,
					url: this.url
				});
			}
		}, 150);
	}

	ngOnDestroy() {
		this.utilService.onUrlChange.emit(null);
	}

	copiar(event) {
		this.clipboardService.copyFromContent(this.utilService.getBaseUrl() + this.url);
		if (window.getSelection && document.createRange) {
			const selection = window.getSelection();
			const range = document.createRange();
			range.selectNodeContents(event.target);
			selection.removeAllRanges();
			selection.addRange(range);
		}
		this.utilService.toast('URL copiada al portapapeles');
	}

	methodClass() {
		return this.method.replace(/\ /g, '');
	}
}
