import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { UtilService } from '@app/core';
import { ClipboardService } from 'ngx-clipboard';
import saveAs from 'file-saver';

@Component({
	selector: 'app-respuesta',
	templateUrl: './respuesta.component.html',
	styleUrls: ['./respuesta.component.scss']
})
export class RespuestaComponent implements OnInit, OnDestroy {

	constructor(
		private utilService: UtilService,
		private clipboardService: ClipboardService
	) { }

	curl = '';
	headers = '';
	respuesta = '{ }';
	certificado = null;
	pdf = null;

	ultimarespuesta = {
		formato: 1,
		logo: '',
		talonarios: []
	};

	cargando = false;

	subServerRequest;
	subResetRespuesta;

	@Output() respuestaEmisor: EventEmitter<any> = new EventEmitter<any>();
	@Input() name: string;


	ngOnInit() {
		this.subServerRequest = this.utilService.onServerRequest.subscribe((request) => {
			if ((this.name && this.name === request.name) || (!this.name)) {
				this.cargarRespuesta(request);
			}
		});
		this.subResetRespuesta = this.utilService.onResetRespuesta.subscribe(() => this.resetRespuesta());
	}

	ngOnDestroy() {
		this.subServerRequest.unsubscribe();
		this.subResetRespuesta.unsubscribe();
	}

	cargarRespuesta(request) {
		if (this.utilService.cuit.length > 0 && this.utilService.ente.length > 0) {
			localStorage.setItem('ente', this.utilService.ente);
			if (this.utilService.validarCuit()) {
				localStorage.setItem('cuit', this.utilService.cuit);
			} else {
				this.utilService.toast('CUIT no válido');
			}
		}
		this.resetRespuesta();
		this.cargando = true;
		request.req.then((response) => {
			if (response) {
				this.curl = request.curl;
				if (response instanceof Blob) {
					saveAs(response, `ventas-${this.utilService.ente}.${response.type.split('/')[1]}`);
					this.respuesta = JSON.stringify({}, undefined, 4);
					this.cargando = false;
					this.headers = '';
					return;
				}
				this.headers = response.headers;
				if (response.body && response.body.csr) {
					this.certificado = response.body.csr;
					response.body.csr = response.body.csr.substring(0, 50) + '...';
				}
				if (response.body && response.body.pdf) {
					this.pdf = response.body.pdf;
					response.body.pdf = response.body.pdf.substring(0, 50) + '...';
				}
				this.respuesta = JSON.stringify(response.body, undefined, 4);
			}
			this.cargando = false;
			this.respuestaEmisor.next(response.body);
		});
	}

	resetRespuesta() {
		this.curl = '';
		this.respuesta = '{ }';
		this.headers = '';
		this.certificado = null;
		this.pdf = null;
	}

	hayRespuesta() {
		return this.curl.length > 0 || this.respuesta.length > 3 || this.headers.length > 0;
	}

	descargar(que) {
		if (que === 'Certificado' && this.certificado) {
			const blob = new Blob([this.certificado], { type: 'text/plain' });
			saveAs(blob, this.utilService.ente + '.csr');
		} else if (que === 'PDF' && this.pdf) {
			const linkSource = `data:application/pdf;base64,${this.pdf}`;
			window.open(linkSource);
		}
	}

	copiar(que) {
		if (que === 'Headers') {
			const div = document.createElement('div');
			div.innerHTML = this.headers;
			this.clipboardService.copyFromContent(div.textContent || div.innerText || '');
		} else if (que === 'PDF') {
			this.clipboardService.copyFromContent('data:application/pdf;base64,' + this[que.toLowerCase()]);
		} else {
			this.clipboardService.copyFromContent(this[que.toLowerCase()]);
		}
		this.utilService.toast(que + ' se copió al portapapeles');
	}

	getRespuesta() {
		return '```json\n' + this.respuesta + '\n```';
	}

}
