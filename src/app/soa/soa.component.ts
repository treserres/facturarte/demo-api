import { Component, ViewChild, ElementRef } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-soa',
	templateUrl: './soa.component.html',
	styleUrls: ['./soa.component.scss']
})
export class SoaComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) { }

	url = '/soa/{cuit}';
	cuitSoa = '';
	@ViewChild('fieldCuit') fieldCuit: ElementRef;

	onConsultar() {
		const request = {
			req: this.httpService.get({}, this.url.replace('{cuit}', this.cuitSoa)),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" "${this.utilService.getBaseUrl()}${this.url.replace('{cuit}', this.cuitSoa)}"`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter(event) {
		if (event.target.name === 'cuit') {
			this.fieldCuit.nativeElement.focus();
		} else {
			this.onConsultar();
		}
	}

	cambiaCuit(ev) {
		if (ev.length !== 11) {
			ev = '{cuit}';
		}
		this.utilService.onUrlChange.emit({
			method: 'get',
			url: this.url.replace('{cuit}', ev)
		});
	}

}
