import { Component } from '@angular/core';
import { UtilService, HttpService } from '@app/core';

@Component({
	selector: 'app-ping',
	templateUrl: './ping.component.html',
	styleUrls: ['./ping.component.scss']
})
export class PingComponent {

	constructor(
		private utilService: UtilService,
		private httpService: HttpService
	) {
		this.utilService.onEnter.subscribe(() => this.onEnter());
	}

	url = '/ping';

	onPing() {
		const request = {
			req: this.httpService.post({}, this.url),
			curl: `curl --header "Authentication:${this.utilService.getAuth()}" ${this.utilService.getBaseUrl()}${this.url}`
		};
		this.utilService.onServerRequest.next(request);
	}

	onEnter() {
		this.onPing();
	}

}
