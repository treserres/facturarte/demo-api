import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpUrlEncodingCodec, HttpClient } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { ClipboardModule } from 'ngx-clipboard';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';

import { PaginaComponent } from './pagina/pagina.component';

import { CrearEmpresaComponent } from './empresas/crear/crear.component';
import { ConsultarEmpresaComponent } from './empresas/consultar/consultar.component';
import { BorrarEmpresaComponent } from './empresas/borrar/borrar.component';
import { AsociarEmpresaComponent } from './empresas/asociar/asociar.component';
import { BorrarEnteComponent } from './empresas/borrar-ente/borrar-ente.component';
import { DesignarRepresentanteComponent } from './empresas/designar/designar.component';
import { RespuestaComponent } from './respuesta/respuesta.component';
import { EntecuitComponent } from './entecuit/entecuit.component';
import { DatosFacturacionComponent } from './empresas/datosf/datosf.component';
import { EmitirFacturaComponent } from './facturacion/emitir/emitir.component';
import { AutocompleteFacturaComponent } from './facturacion/autocomplete/autocomplete.component';
import { ConsultarFacturacionComponent } from './facturacion/consultar/consultar.component';
import { UrlComponent } from './url/url.component';
import { PingComponent } from './ping/ping.component';
import { PedirCertificadoComponent } from './afip/pedir-certificado/pedir-certificado.component';
import { EstadoComponent } from './afip/estado/estado.component';
import { SoaComponent } from './soa/soa.component';
import { DgrComponent } from './dgr/dgr.component';
import { CoreModule } from './core';

export const MY_FORMATS = {
	parse: {
		dateInput: 'DD/M/YYYY',
	},
	display: {
		dateInput: 'DD/MM/YYYY',
		monthYearLabel: 'MMMM YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'MMMM YYYY',
	},
};

@NgModule({
	declarations: [
		AppComponent,
		PaginaComponent,
		CrearEmpresaComponent,
		ConsultarEmpresaComponent,
		BorrarEmpresaComponent,
		AsociarEmpresaComponent,
		BorrarEnteComponent,
		DesignarRepresentanteComponent,
		RespuestaComponent,
		EntecuitComponent,
		DatosFacturacionComponent,
		EmitirFacturaComponent,
		ConsultarFacturacionComponent,
		AutocompleteFacturaComponent,
		UrlComponent,
		PingComponent,
		PedirCertificadoComponent,
		EstadoComponent,
		SoaComponent,
		DgrComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		CoreModule,
		FormsModule,
		AppRoutingModule,
/*		MatSnackBarModule,
		MatToolbarModule,
		MatSidenavModule,
		MatIconModule,
		MatButtonModule,
		MatListModule,
		MatSelectModule,
		MatInputModule,
		MatFormFieldModule,
		MatExpansionModule,
		MatBottomSheetModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatRippleModule,
		MatTabsModule,
		MatSlideToggleModule,
		MatTooltipModule,
		MatDividerModule,
		MatTableModule,
		MatDatepickerModule,
		MatDatepickerModule,
		MatMomentDateModule,
		MatMomentDateModule,
		MatCheckboxModule,
*/
		ClipboardModule,
		ScrollingModule,
		SharedModule,
		MarkdownModule.forRoot({
			loader: HttpClient,
			markedOptions: {
				provide: MarkedOptions,
				useValue: {
					gfm: true,
					tables: true,
					breaks: false,
					pedantic: false,
					sanitize: false,
					smartLists: true,
					smartypants: false,
				},
			},
		}),
	],
	providers: [
		HttpUrlEncodingCodec
	],
	bootstrap: [AppComponent],
})
export class AppModule { }
