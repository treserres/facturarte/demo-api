Solicita un Certificate Signing Request.

* Si la empresa no tiene asignada una clave privada, genera internamente una nueva clave privada y la almacena encriptada en la base de datos de facturarte.
* Con la clave privada ya generada, responde con un certificado CSR para solicitar la firma ante AFIP.

### Request

* La cabecera deberá incluir Autenticación .
* Se pasa la correspondiente CUIT y el ente que solicita el certificado.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"cuit": 30648802680,
	"ente": "aaaaaa"
}
```

### Response

**En caso de certificado creado**, retorna el csr en formato de texto.

```http
Status: 201 Created
```
```json
{
	"csr" : "-----BEGIN CERTIFICATE REQUEST----- MIIBpDCCAQ0CAQAwZDELMAkGA1UEBhMCQVIxIDAeBgNVBAoMF1J1YmVuX1JpdmFz X3lfQXNvY2lhZG9zMRgwFgYDVQQDDA9wbXJpdmFzLm1pbmUubnUxGTAXBgNVBAUT EENVSVQgMzA2NDg4MDI2ODAwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAOMV tpHsBuFhXl/PW6Gs/taHz+prn8Qm/9iE3EEWP8RGyAi7kvIr5JMlEd/BVyF/6L/v VKyUDOc5URlQMkgy/ILaFOm8eoVF3cuoqOl39ahsKzp1PuFzfZLxdA== -----END CERTIFICATE REQUEST-----"
}
```

**En caso de certificado generado anteriormente**, es decir, ya se realizó la creación del certificado retorna la misma información con distinto header:

```http
Status: 202 Accepted
```
```json
{
	"csr" : "-----BEGIN CERTIFICATE REQUEST----- MIIBpDCCAQ0CAQAwZDELMAkGA1UEBhMCQVIxIDAeBgNVBAoMF1J1YmVuX1JpdmFz X3lfQXNvY2lhZG9zMRgwFgYDVQQDDA9wbXJpdmFzLm1pbmUubnUxGTAXBgNVBAUT EENVSVQgMzA2NDg4MDI2ODAwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAOMV tpHsBuFhXl/PW6Gs/taHz+prn8Qm/9iE3EEWP8RGyAi7kvIr5JMlEd/BVyF/6L/v VKyUDOc5URlQMkgy/ILaFOm8eoVF3cuoqOl39ahsKzp1PuFzfZLxdA== -----END CERTIFICATE REQUEST-----"
}
```

**En caso de empresa  