Este método devuelve el número de inscripción de Ingresos Brutos en la Pampa de un contribuyente..

### Request


```json
{
    cuit: 30648802680
}
```


### Response

Responde nuevamente la cuit y su código de Ingresos Brutos (Número más dígito de control).

```http
Status: 200 OK
```
```json
{
        cuit:"30648802680",
        iibb:"164634/8"
}
```

En caso de no figurar como inscripto en el Padrón, responde con el código en false y el encabezado de OK.

```http
Status: 200 OK
```
```json
{
        cuit: "306488",
        iibb: false
}
```