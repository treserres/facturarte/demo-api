Este método trae los datos del padrón PUC Nivel 4.  Siempre utiliza nuestro certificado de producción, es de acceso público y limitado por ip proveedora.  En caso de proveer certificados de cliente los límites por ip se expanden.

### Request


```json
{
		cuit: 30648802680
}
```


### Response

Responde tal cual los parámetros del PUC Nivel 4.

```http
Status: 200 OK
```
```json
{
	 "success":1,
	 "data":{
			"domicilioFiscal":{
				 "codPostal":"6300",
				 "localidad":"SANTA ROSA",
				 "direccion":"L.V.MANSILLA 280",
				 "idProvincia":21
			},
			"domicilioReal":{
				 "codPostal":"6300",
				 "localidad":"SANTA ROSA",
				 "direccion":"EMILIO MITRE 100",
				 "idProvincia":21
			},
			"impuestos":[
				 103,
				 30,
				 301,
				 211,
				 10
			],
			"telefono":422993,
			"email":"treserres@pmrivas.com",
			"nombre":"RUBEN R RIVAS Y ASOCIADOS SOC COLECTIVA",
			"idPersona":30648802680,
			"todas":{
				 "actividad":[
						{
							 "descripcionActividad":"SERVICIOS DE CONTABILIDAD, AUDITOR\u00cdA Y ASESOR\u00cdA FISCAL",
							 "idActividad":692000,
							 "nomenclador":883,
							 "orden":1,
							 "periodo":201311
						},
						{
							 "descripcionActividad":"APICULTURA",
							 "idActividad":14910,
							 "nomenclador":883,
							 "orden":2,
							 "periodo":201311
						},
						{
							 "descripcionActividad":"SERVICIOS DE CONSULTORES EN INFORM\u00c1TICA Y SUMINISTROS DE PROGRAMAS DE INFORM\u00c1TICA",
							 "idActividad":620100,
							 "nomenclador":883,
							 "orden":3,
							 "periodo":201507
						}
				 ],
				 "domicilio":[
						{
							 "codPostal":"6300",
							 "descripcionProvincia":"LA PAMPA",
							 "direccion":"L.V.MANSILLA 280",
							 "idProvincia":21,
							 "localidad":"SANTA ROSA",
							 "tipoDomicilio":"FISCAL"
						},
						{
							 "codPostal":"6300",
							 "descripcionProvincia":"LA PAMPA",
							 "direccion":"EMILIO MITRE 100",
							 "idProvincia":21,
							 "localidad":"SANTA ROSA",
							 "tipoDomicilio":"LEGAL\/REAL"
						}
				 ],
				 "email":[
						{
							 "direccion":"treserres@pmrivas.com",
							 "estado":"No Verificado",
							 "tipoEmail":"COMERCIAL"
						}
				 ],
				 "estadoClave":"ACTIVO",
				 "fechaContratoSocial":"1992-02-03T12:00:00-02:00",
				 "fechaInscripcion":"1992-02-21T12:00:00-02:00",
				 "formaJuridica":"SOC. COLECTIVA",
				 "idPersona":30648802680,
				 "impuesto":[
						{
							 "descripcionImpuesto":"REGIMENES DE INFORMACI\u00d3N",
							 "diaPeriodo":1,
							 "estado":"ACTIVO",
							 "ffInscripcion":"2011-08-03T12:00:00-03:00",
							 "idImpuesto":103,
							 "periodo":200701
						},
						{
							 "descripcionImpuesto":"ACTIVOS SOCIEDADES L.23905",
							 "diaPeriodo":1,
							 "estado":"BAJA DEFINITIVA",
							 "ffInscripcion":"1992-02-01T12:00:00-02:00",
							 "idImpuesto":186,
							 "periodo":199506
						},
						{
							 "descripcionImpuesto":"IVA",
							 "diaPeriodo":1,
							 "estado":"ACTIVO",
							 "ffInscripcion":"1992-02-01T12:00:00-02:00",
							 "idImpuesto":30,
							 "periodo":199202
						},
						{
							 "descripcionImpuesto":"EMPLEADOR-APORTES SEG. SOCIAL",
							 "diaPeriodo":1,
							 "estado":"ACTIVO",
							 "ffInscripcion":"1992-01-01T12:00:00-02:00",
							 "idImpuesto":301,
							 "periodo":199201
						},
						{
							 "descripcionImpuesto":"BP-ACCIONES O PARTICIPACIONES",
							 "diaPeriodo":5,
							 "estado":"ACTIVO",
							 "ffInscripcion":"2003-05-23T12:00:00-03:00",
							 "idImpuesto":211,
							 "periodo":200305
						},
						{
							 "descripcionImpuesto":"GANANCIAS SOCIEDADES",
							 "diaPeriodo":1,
							 "estado":"ACTIVO",
							 "ffInscripcion":"2016-05-16T12:30:18-03:00",
							 "idImpuesto":10,
							 "periodo":201501
						}
				 ],
				 "mesCierre":12,
				 "porcentajeCapitalNacional":100,
				 "razonSocial":"RUBEN R RIVAS Y ASOCIADOS SOC COLECTIVA",
				 "telefono":[
						{
							 "numero":422993,
							 "tipoLinea":"FIJO",
							 "tipoTelefono":"CAMPA\u00d1A TELEF\u00d3NICA"
						}
				 ],
				 "tipoClave":"CUIT",
				 "tipoPersona":"JURIDICA"
			}
	 }
}
```

* En caso de una cuit inexistente, retorna 204 

```http
Status: 204 No Content
```

* Llamada de prueba, utilizando la cuit de Ruben R. Rivas y Asociados SC: [Padron Afip ](https://facturarte.com.ar/apiv2/soa/30648802680)
* Llamada de prueba, utilizando una cuit inválida: [ Padron inválido ](https://facturarte.com.ar/apiv2/soa/3064880)