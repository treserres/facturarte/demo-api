Este método permite asociar un nuevo ente a una empresa ya registrada. Permite que mas de un ente del mismo prestador emita facturas.  Para confirmar que el ente fue autorizado requerimos que transmita nuevamente el certificado firmado por afip.

### Request

* La cabecera deberá incluir Autenticación .
* Los parámetros son la cuit de la empresa, el ente, y el correspondiente crt.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	cuit: '30648802680',
	ente: 'abcdefghijkl',
	crt: '-----BEGIN CERTIFICATE----- ... -----END CERTIFICATE-----'
}
```

### Response

**En caso de que el certificado sea válido** devolverá status 200 OK con los datos del certificado (issuer, validez)

```http
Status: 200 OK
```
```json
{
	"datoscrt": {
			"nombre": "/CN=facturArte/serialNumber=CUIT 20229360249",
			"alias": "facturArte",
			"cuit": "CUIT 20229360249",
			"issuer": "Computadores Test",
			"validez": "28/10/2020 13:57:24"
		},
		"keyok": 1
}
```

**En caso de que el certificado NO coincida con la clave privada** devolverá el error 409 con el siguiente detalle:
```http
Status: 409 Conflict
```
```json
{
	 "err":  1011,
	 "txerr":  "Certificado NO Coincide con la clave privada"
}
```

**En caso de que falte autorizar en afip la emisión de facturas electrónicas** devolverá el error 409 con el siguiente detalle:
```http
Status: 409 Conflict
```
```json
{
	 "err":  409,
	 "txerr":  "ns1: not authorized"
}
```
