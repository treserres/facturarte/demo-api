En todos los casos de interacción con el sistema se deberán intercambiar autenticación (explicado en [Autenticación](autenticacion)), y un código de ente.

El ente podría entenderse como el usuario originante del requerimiento.  Esto es requerido dado que el único usuario y contraseña que se intercambia con el sistema es el del prestador, entonces, puede ocurrir que un usuario del sistema del prestador tenga varias empresas, o que una empresa tenga más de un usuario en el sistema del prestador.

## Glosario:

#### Prestador

- Definimos como prestador al sitio web o empresa prestadora del servicio de facturación.

#### Ente:

- En términos prácticos, el ente es el usuario registrado en el prestador.  El código de ente (alfanumérico de hasta 64 caracteres) debe ser único e irrepetible en el prestador.

#### Empresa:

- Empresa es toda persona física o jurídica que tenga CUIT válido, único e irrepetible y que esté en condiciones de emitir facturas electrónicas.

#### UID y KEY:

- Uid, campo numérico, KEY: campo alfanumérico de hasta 32 caracteres que componen la autenticación del prestador para acceder a la api.

#### Puesto de Ventas:

- Es un código numérico de hasta 4 dígitos definido por AFIP y es el prefijo de cualquier factura emitida.

## Para tener en cuenta:

- La relación Ente - Empresa debe ser tenida en cuenta por el prestador.  En el caso de un alta de empresa nueva el sistema exige al ente (a través del prestador) que presente un certificado válido, firmado por AFIP.  Este proceso nos permite asegurar de que el ente está en condiciones de emitir facturas utilizando nuestro sistema.
- En los casos de empresas con mas de un ente, al asociar un segundo ente a la misma empresa el sistema volverá a solicitar el mismo certificado.  De esta manera nuestro sistema se asegura que el ente solicitante tenga autorización para emitir facturas.
- Es importante que el prestador tenga en cuenta proteger la identificación del ente emisor de comprobantes.
