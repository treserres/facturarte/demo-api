### Error

La siguiente es una enumeración de los códigos de error del sistema.  Las cabeceras pueden variar de acuerdo a la implementación:

    - 401: Acceso denegado, error de UID o token del PRESTADOR.
    - 1001: Esa key no coincide con el csr almacenado.
    - 1002: La cuit representante no ha sido autorizada por el representado
    - 1003: La cuit representante no cuenta con certificado en nuestras bases de datos
    - 1004: Solo se podrá verificar en XX horas
    - 1005: Certificado no aceptado por afip
    - 1006: Afip OFF LINE
    - 1007: Empresa No Existe en nuestra bd.
    - 1008: Empresa Asignada a OTROS entes.
    - 1009: Errores Varios de AFIP (se aclaran en la respuesta)
    - 1010: Datos Nulos o no aceptados (se aclaran en la respuesta)
    - 1011: Certificado no coincide con la clave privada.
    