### Request POST, PUT, PATCH

* La cabecera deberá incluir Autenticación .
* Se pasan los datos que se desea incorporar (se pueden utilizar indistintamente los tres métodos)

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"cuit": '30648802680',
	"talonario": {
		"prefijo":"0002",
		"defecto":true,
		"manual":false
		},
	"logo": "data:image/png;base64,TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhp..."
}
```

### Response

**En caso de empresa inexistente o cuit erróneo**, retorna 204

```http
Status: 204 No Content
```

**En caso de talonario/logo creado**, (si el método fue POST, o PUT) retorna 201 
```http
Status: 201 Created
```
```json
{
	 "id": 1234,
	 "talonarios":[
	 		{
	 			"prefijo":"0001",
	 			"defecto":false,
	 			"manual":false,
	 			"ultimo":"00123456"
	 		},
	 		{
	 			"prefijo":"0002",
	 			"defecto":true,
	 			"manual":false,
	 			"ultimo":"00000000"
	 		}
	 	]
}
```

**En caso de talonario/logo modificado**, (si el método fue POST o PATCH) retorna 202
```http
Status: 202 Accepted
```
```json
{
	 "id": 1234,
	 "talonarios":[
	 		{ ... } ]
}
```

**En caso de inconsistencia**, retorna 204 .

* Este es el caso, por ejemplo, de utilizar PUT en un talonario existente, o PATCH en un talonario inexistente..

```http
Status: 204 No Content
```

El sistema siempre devuelve la lista completa de talonarios asignados y los numeradores (campo ultimo) actuales.  El numerador no es requerido en los métodos POST, PUT, PATCH


### Request DELETE

* La cabecera deberá incluir Autenticación .
* Se pasan los datos de los talonarios que se desean borrar, solo es requerido el campo prefijo, en caso de que se pasen mas datos se chequea la concordancia (excepto de numeradores)
* En caso de logo, simplemente pasando como parámetro logo con el enctype o cualquier dato...

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"cuit": '30648802680',
	"talonario": {
		"prefijo":"0002"
		}
}
```

### Response

**En caso de empresa inexistente o cuit erróneo**, retorna 204

```http
Status: 204 No Content
```

**En caso de talonario borrado**, es decir, en caso correcto
```http
Status: 202 Accepted
```
```json
{
	 "id": 1234,
	 "talonarios":[
	 		{
	 			"prefijo":"0001",
	 			"defecto":false,
	 			"manual":false,
	 			"ultimo":"00123456"
	 		},
	 		{
	 			"prefijo":"0002",
	 			"defecto":true,
	 			"manual":false,
	 			"ultimo":"00000000"
	 		}
	 	],
	"logo": null | "data:image/png;base64,TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhp..."
}
```
