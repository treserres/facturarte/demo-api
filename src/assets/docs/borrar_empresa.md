Este método destruye por completo una empresa con todos sus datos incluyendo toda su información, certificados, entes asociados, etc.

### Request

* La cabecera deberá incluir Autenticación .
* Se ingresa la cuit correspondiente a la empresa, y el ente que la solicita.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	cuit: '30648802680',
	ente: 'abcdefghijkl'
}
```

### Response

**En caso de empresa existente y borrada**, Destruye todos los datos de la empresa, sus entes asociados, certificados, etc..

```http
Status: 200 Ok
```
```json
{
	 "id": 1234,
	 "success":1,
	 "Borrada":1
}
```

**En caso de empresa inexistente ó cuit erróneo**, retorna 204

```http
Status: 204 No Content
```

**En caso de cuit registrado pero de otro ente**, retorna status 409 Conflict, código de ID y un array con los entes ya registrados a esa empresa. 
```http
Status: 409 Conflict
```
```json
{
	 "id": 1234,
	 "entes" : ["123123","123123","33333"],
}
```

**En caso de ente asociado a mas empresas**, Desvincula el usuario, pero retorna 202 e informa las empresas a las q sigue vinculado.  Si además la empresa tenía mas entes asociados, agrega los entes a la respuesta

```http
Status: 202 Accepted
```
```json
{
	 "id": 1234,
	 "empresas" : ["30648802680","30123456789"],
	 "entes" : ["656565","645456"]
}
```

**En caso de empresa existente con mas entes**, Destruye todos los datos del usuario, pero retorna 202 e informa los entes

```http
Status: 202 Accepted
```
```json
{
	 "id": 1234,
	 "entes" : ["123123","123123","33333"],
}
```
