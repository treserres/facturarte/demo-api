Este método busca en nuestra base de datos de cuits de afip (provista por [afip en este link](http://www.afip.gob.ar/genericos/cInscripcion/archivos/apellidoNombreDenominacion.zip), descripción en [este link](http://www.afip.gob.ar/genericos/cInscripcion/archivoCompleto.asp) ).

Se pasan los datos de autenticación, cuit y ente para que el sistema de prioridad a las facturas YA emitidas por ese ente.

Tener en cuenta que la respuesta informa los datos propios de afip que figuran en esa base de datos, por lo tanto, para saber el tipo de comprobante que se deberá emitir (A, B), es aconsejable, una vez obtenido un cuit válido, solicitar el correspondiente "SOA" pasando la cuit como parámetro.

### Request


```json
{
	"ente": "Usuario123",
	"cuit": 20229360249
	"texto": "Fernand"
}
```


### Response

Responde con la siguiente información (con un límite de 30 cuits propios y otros 30 cuits de afip), de acuerdo a la siguiente estructura:

id: Cuit
label: Cuit y Razon Social
value: Cuit
razsoc: Razón Social de la empresa
ganancias, iva, sociedad, monotr: Texto que indica la condición:

'NI', 'N'	No Inscripto
'AC', 'S'	Activo
'EX'	Exento
'NA'	No alcanzado
'XN'	Exento no alcanzado
'AN'	Activo no alcanzado
'NC'	No corresponde

catasa: para percepciones ARBA, tasa publicada de percepción para esa cuit
hay: 0 -> No se han emitido facturas a esa cuit en nuestros registros, 1 -> Se han emitido facturas a esa cuit

facturar: indica el tipo de factura a emitir de acuerdo a la condicion de iva del ente.

#### A tener en cuenta:
La longitud máxima del campo razón social es de apenas 30 caracteres, y la base de datos de afip se pide una vez por mes, por lo que en muchos casos, para obtener bien la información completa es conveniente llamar a la función SOA una vez obtenida la cuit para obtener los datos correctos de una razón social.


```http
Status: 200 OK
```
```json
[
	{
		"id":"30716097842",
		"label":"30716097842-FERNANDA ALIMENTOS ORGANICOS Y",
		"value":"30716097842",
		"razsoc":"FERNANDA ALIMENTOS ORGANICOS Y",
		"ganancias":"AC",
		"iva":"AC",
		"monotr":"NI",
		"sociedad":"N",
		"empleador":"S",
		"hay":"0",
		"catasa":"2.50",
		"facturar":"A"
	},
	{
		"id":"30715726498",
		"label":"30715726498-FERNANDA BALLESTER Y MARIA EUG",
		"value":"30715726498",
		"razsoc":"FERNANDA BALLESTER Y MARIA EUG",
		"ganancias":"AC",
		"iva":"NI",
		"monotr":"B",
		"sociedad":"N",
		"empleador":"S",
		"hay":"0",
		"catasa":"0.00",
		"facturar":"B"
	},
]
```