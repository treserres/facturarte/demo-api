Permite verificar el estado de AFIP y de la información del certificado de una cuit

### Request

* La cabecera deberá incluir Autenticación. Como parámetro se pasan la cuit.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"cuit": 20229360249
}
```

### Response

* El sistema chequea la existencia de certificados en nuestras bases de datos e intenta hacer un llamado a AFIP probando la consistencia de la información suministrada.  
**En caso de certificado correcto**, retorna error 0 y las fechas de validez del certificado del representante

```http
Status: 200 OK
```
```json
{
	"err": 0,
	"desde": "2015-04-08 15:03:21",
	"hasta": "2018-02-01 15:03:21",
	"certificado_cargado": true,
    "certificado_afip": true
}
```


**En caso de rechazo de AFIP**, es decir que AFIP rechaza el certificado

**En caso de que no exista key**, es decir que nuestras bases de datos no cuentan con el correspondiente certificado:

```http
Status: 400 Access denied
```
```json
{
	"err": 1003,
	"txerr": "La cuit representante no cuenta con certificado en nuestras bases de datos"
}
```

```http
Status: 400 Access denied
```
```json
{
	"desde": "2018-12-13 09:40:16",
	"hasta": "2018-12-13 21:40:16",
	"certificado_cargado": true,
	"err": 1005,
	"txerr": "Certificado no aceptado por AFIP"
}
```
