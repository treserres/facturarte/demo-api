* Permite especificar que CUIT es su representante para emisión de facturación electrónica.
* El representante deberá contar con certificado en nuestros servidores.

### Request

* La cabecera deberá incluir Autenticación .
* Como parámetros se pasan la cuit del representante y la cuit del representado.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"representante": '30648802680',
	"representado": '20229360249'
}
```

### Response

* El sistema chequea la existencia de certificados en nuestras bases de datos e intenta hacer un llamado a AFIP probando la consistencia de la información suministrada.
**En caso de certificado correcto**, retorna el OK y las fechas de validez del certificado del representante

```http
Status: 201 Created
```
```json
{
	"Ok":1,
	"Desde":"2015-04-08 15:03:21",
	"Hasta":"2018-02-01 15:03:21"
}
```

**En caso de certificado correcto ya existente**, es decir, ya se realizó la vinculación correcta, devuelve la misma información con otra cabecera:

```http
Status: 202 Accepted
```
```json
{
	"Ok":1,
	"Desde":"2015-04-08 15:03:21",
	"Hasta":"2018-02-01 15:03:21"
}
```

**En caso de rechazo de AFIP**, es decir que AFIP rechaza la vinculación (o no se ha realizado todavía):

```http
Status: 403 Access denied
```
```json
{
        "err": 1002,
        "txerr": "La cuit representante no ha sido autorizada por el representado"
}
```

**En caso de que no exista key**, es decir que nuestras bases de datos no cuentan con el correspondiente certificado:

```http
Status: 403 Access denied
```
```json
{
        "err": 1003,
        "txerr": "La cuit representante no cuenta con certificado en nuestras bases de datos"
}
```

**En caso de no poder chequear**, AFIP establece un límite de keys disponibles y si se establecen demasiadas relaciones (en estos momentos más de dos) no se puede evaluar la relación de claves fiscales. En ese caso se deberá esperar a que expire la autorización de AFIP

```http
Status: 403 Access denied
```
```json
{
        "err": 1004,
        "txerr": "Sólo se podrá verificar en 12 horas"
}
```
