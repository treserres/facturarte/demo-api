### Éxito

Los codigos de respuesta de éxito confirman la recepción íntegra de los datos de la transacción :

* `GET`, `PUT`, `DELETE` y `POST` retornan en caso de éxito el código `200 OK`,
* `POST ` puede retornar el código `201 CREATED`, o el código `202 ACCEPTED` según corresponda a la operación

A modo de ejemplo, buscando un CUIT en la [DGR La Pampa](dgr):

```http
Status: 200 OK
```
```json
{
        cuit:"30648802680",
        iibb:"164634/8"
}
```

* Pero en caso de informar un cuit inexistente o inválido la respuesta tambíen será encabezada con un `200 OK` dado que la transacción puede ser ejecutada aunque no retorne información útil:

```http
Status: 200 OK
```
```json
{
        cuit:"30648802",
        iibb:false
}
```

### Error

Para los códigos de error utilizamos los [códigos HTTP standard ](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html) tratando de brindar información adicional en json:

* El error se retorna en la cabecera,
* El cuerpo retorna además el campo err con el código y el campo txerr con el texto en español de la descripción del error,

Por ejemplo, un ping con error de credenciales:

```http
Status: 401 Access denied
```
```json
{
    err: 401,
    txerr: 'Acceso denegado, error de uid / token.'
}
```

* En caso de acciones prohibidas por la configuración del sistema se podrán encontrar casos de `403 Forbidden`.

```http
Status: 403 Forbidden
```
```json
{
    err: 403,
    txerr: 'El usuario no puede realizar esta acción.'
}
```

* El error 404 solo se utiliza en caso de recursos inexistentes.

```http
Status: 404 Not Found
```
```json
{
    err: 404,
    txerr: 'Recurso inexistente'
}
```

* En casos muy específicos el servidor podrá retornar con un `409 Conflict`. Por ejemplo, al intentar modificar la cuit de una empresa y asignar la cuit de otra empresa existente:

```http
Status: 409 Conflict
```
```json
{
        cuit:"30648802680",
        err: 1234,
        txerr: "Ya existe otra empresa registrada con esa cuit"
}
```