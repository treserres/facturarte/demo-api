Este método consulta los datos de una empresa ya dada de alta

### Request

* La cabecera deberá incluir Autenticación.
* Se ingresa la cuit correspondiente a la empresa, y el ente que la solicita.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	cuit: '30648802680',
	ente: 'abcdefghijkl'
}
```

### Response

**En caso de empresa existente**, y registrada para ese ente, retorna 200 OK, los datos del padrón PUC de la empresa, y un código único e irrepetible de ID.

```http
Status: 200 OK
```
```json
{
	 "id":  1234,
	 "success": 1,
	 "data": {
			"domicilioFiscal": {
				...
				}
			}
}
```

**En caso de empresa existente**, pero nunca registrada a un ente, retorna 200 y los datos de la empresa (sin código de id):

```http
Status: 200 OK
```
```json
{
	 "success": 1,
	 "data": {
			"domicilioFiscal": {
				...
				}
			}
}
```


**En caso de empresa inexistente ó cuit erróneo**, retorna 204

```http
Status: 204 No Content
```

**En caso de cuit registrado pero de otro ente**, retorna status 409 Conflict, con la información de la empresa, código de ID y un array con los entes ya registrados a esa empresa. 
```http
Status: 409 Conflict
```
```json
{
	 "id":  1234,
	 "entes": ["123123","123123","33333"],
	 "success": 1,
	 "data": {
			"domicilioFiscal": {
				...
				}
			}
}
```

Para dar de alta un nuevo ente y asociarlo a esa empresa, deberá utilizar el método [Asociar Ente](empresa/asociar)).

Es importante tener en cuenta lo siguiente... Hay que tener cuidado con este punto, dado que el sistema permite a mas de un ente asociarse a una empresa.  Esto podría generar robos de identidad, dado que la única pregunta al ente es la cuit de la empresa a asociar.  Por lo que recomendamos tener cuidado en la implementación de más de un ente asociado a una empresa.

Como medida adicional, para asociar un nuevo ente a una empresa solicitamos el archivo crt (que solo se obtiene por AFIP).
