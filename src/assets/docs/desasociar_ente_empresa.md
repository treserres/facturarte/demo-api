Este método permite borrar un ente de una empresa ya registrada. Quita definitivamente al ente de su relación con la empresa

### Request

* La cabecera deberá incluir Autenticación .
* Los parámetros son la cuit de la empresa, el ente.

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	"cuit": "30648802680",
	"ente": "Usuario1"
}
```

### Response
