La mayoría de los requerimientos exigen autenticación. En el caso de métodos públicos que no requieran autenticación, en caso de ser incluída, será analizada y en caso de credenciales no coincidentes arrojará el correspondiente error `401 Unauthorized`

### Request

* La cabecera deberá incluir las credenciales UID y KEY separadas por el símbolo `:` codificados en base64.  Ejemplo: UID 123, KEY: a654a654a654, el resultado sería `MTIzOmE2NTRhNjU0YTY1NAo=` 


```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```


### Response

* En cualquier caso la respuesta negativa será el error `401 Unauthorized`.
* Existen métodos para el cambio de parámetros de autenticación y estos se aplican en forma automática para nuevos requerimientos, consulte [Cambio de KEY](cambiokey) para más información sobre cambios de clave de autenticación.

Para errores de respuesta, consulte [Codigos de Respuesta](codigosrespuesta).
