Consulta de facturas emitidas, paginadas y filtradas por texto y rango de fecha. Se puedem consultar las facturas de toda la empresa o sólo las correspondientes al ente especificado. Si se especifica un formato diferente de JSON (0), se retorna el archivo correspondiente.

### Request

* La cabecera deberá incluir Autenticación.
El cuerpo se conforma de la siguiente manera:

"**ente**": *(string)* se debe especificar, aun si no se filtran las facturas que corresponden al ente.

"**cuit**": *(int)* cuit de la empresa.

"**desde**": *(string, d/m/Y)* fecha de comienzo de la consulta.

"**hasta**": *(string, d/m/Y)* fecha de fin de la consulta.

"**pagina**": *(int)* especifica la página cuando se elige el formato JSON para paginar resultados (10 por página).

"**limitar**": *(bool 0,1)* filtrar facturas por ente especificado.

"**formato**": *(int)* puede ser uno de los siguientes valores:


| Formato                 | Código |
|-------------------------|--------|
| JSON                    | 0      |
| CSV separado por ;      | 1      |
| CSV separado por ,      | 2      |
| Completo separado por , | 3      |
| RCEL                    | 4      |
| Completo separado por ; | 5      |
| IVA Ventas              | 6      |
| Holistor                | 7      |
| Todas en PDF            | 10     |
| Libro IVA Ventas        | 11     |
| Siarib                  | 12     |

&nbsp;

##### Request válida #1

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo= 
```
```json
{
	"ente": "Usuario1",
	"cuit": "30648802680",
    "formato": 0,
    "desde": "01/12/2018",
    "hasta": "31/12/2018",
    "pagina": 1,
    "limitar": 1
}
```

&nbsp;

----

&nbsp;
##### Request válida #2

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo= 
```
```json
{
	"ente": "Usuario1",
	"cuit": "30648802680",
    "formato": 1,
    "desde": "01/12/2018",
    "hasta": "31/12/2018",
    "limitar": 0
}
```
&nbsp;

### Response

```json
{
    "err": 0,
    "facturas": [
        {
            "emrzabrevia": null,
            "efemid": "24859",
            "efprinttst": "2018-12-10 10:54:23",
            "effecha": "2018-12-10",
            "txeffecha": "10/12/18",
            "efcomprob": "0001-00000002",
            "efpacodigo": "FC",
            "efrazsocial": "JUAN PEREZ",
            "efid": "98964",
            "efcuitsg": "27306981035",
            "total": "10.00",
            "efactiva": "10",
            "efncefid": "0",
            "cvatributos": null,
            "cvdescr": null,
            "imputado": "0.00",
            "efimid": "1",
            "status": null,
            "statusemail": null,
            "efcvid": "1",
            "eacodigo": null,
            "emcuit": "20-22936024-9",
            "renglones": [
                {
                    "mvardescr": "art",
                    "mvcantar": "1.000",
                    "mvunitario": "10.00",
                    "mvtotal": "10.00"
                }
            ]
        }
    ],
    "total": 1,
    "pagina": 1,
    "por_pagina": 10
}
```