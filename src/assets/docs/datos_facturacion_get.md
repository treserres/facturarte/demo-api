
Este método consulta o establece los datos de facturación de una empresa. Como una empresa puede tener distintos talonarios en un ente, si se pasa el ente, trae los asignados al ente, si se pasa solo la empresa, trae todos los talonarios.


### Request GET

* La cabecera deberá incluir Autenticación .
* El único parametro a ingresar es la correspondiente cuit

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo= 
```
```json
{
	"ente": "Usuario123",
	"cuit": 20229360249
}
```

### Response

**En caso de empresa existente**, retorna los datos de facturación de una empresa

```http
Status: 200 OK
```
```json
{
	"id": 1234,
	"formato": 1,
	"talonarios": [
		{
			"prefijo":"0001",
			"defecto":true,
			"manual":false,
			"ultimo":"00123456"
		},
		{
			"prefijo":"0002",
			"defecto":false,
			"manual":false,
			"ultimo":"00000000"
		}
	],
	"dtsEmpresa": {
		"emrazsocial": "RIVAS PABLO MANUEL",
        "emnomfant": "Tres Erres",
        "emdomicilio": "TOMAS MASON 246",
        "emcodpost": "6300",
        "emlocalidad": "SANTA ROSA",
        "emprovincia": "LA PAMPA",
        "emtelefono": "02954437890",
        "eminiact": "01/01/2016",
        "emibt": "123456",
        "ememail": "pmrivas@gmail.com",
        "tldefecto": "0002",
        "logo": null
	},
}
```

**En caso de empresa inexistente o cuit erróneo**, retorna 204

```http
Status: 204 No Content
```
