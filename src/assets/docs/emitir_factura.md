Emisión de una factura, pasando todos los datos necesarios y obteniendo el archivo PDF del comprobante


### Request

* La cabecera deberá incluir Autenticación .

El cuerpo se conforma del cuit de la entidad a facturar y su denominación ó su denominación y DNI

Se incorpora tambien opcional el campo "tipoFactura", en el que se puede especificar: F (Factura), C (Nota de Credito), D (nota de Débito), o se puede especificar tambien la letra (es opcional, el sistema reconoce automáticamente la correcta, pero si se "fuerza" el sistema respetará la opcion): FA (Factura A), FB (Factura B), FC (Factura C), FM (Factura M), CA ( Crédito A), CB (Crédito B).... (DA Débito A) y así sucesivamente...

Adicional: enviarMail y mailFactura

. enviarMail: Opcional, acepta true o false.

. mailFactura: Dirección de email a la que enviar la factura...

* Este correo electrónico se envía desde nuestra dirección: sistema@facturarte.com.ar


Dos request válidas serían las siguientes:

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo= 
```
```json
{
	"ente": "Usuario1",
	"cuit": "30648802680",
	"cuitFactura": "30648802680",
	"tipoFactura": "F",
	"denominacionFactura": "RUBEN R RIVAS Y ASOCIADOS SOC COLECTIVA",
	"articulos": [
		{
			"detalle": "Articulo1",
			"cantidad": 1,
			"precio": 10.50,
			"iva": 21
		},
		{
			"detalle": "Articulo2",
			"cantidad": 1,
			"precio": 15,
		}
	]
}
```

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo= 
```
```json
{
	"ente": "Usuario1",
	"cuit": "30648802680",
	"denominacionFactura": "RIVAS PABLO MANUEL",
	"dniFactura": "22936024",
	"enviarMail": true,
	"mailFactura": "me@mehost.com",
	"articulos": [
		{
			"detalle": "Articulo1",
			"cantidad": 1,
			"precio": 10.50,
			"iva": 0
		}
	]
}
```

### Response

