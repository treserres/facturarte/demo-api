Este método crea una nueva empresa en el sistema facturarte. Verifica la consistencia de la CUIT y la su existencia en el padrón de AFIP.  No se conocen casos de cuits que no existan en las bases de afip, pero en caso de una cuit válida e inexistente permite la creación de la empresa y supone que es unipersonal y cuit en trámite.

### Request

* La cabecera deberá incluir Autenticación .
* Los parámetros son, CUIT y ente que solicita su creación:

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```
```json
{
	cuit: '30648802680',
	ente: 'abcdefghijkl'
}
```

### Response

**En caso de empresa creada**, retorna los datos del padrón PUC de la empresa, y un código único e irrepetible de ID.

```http
Status: 201 Created
```

```json
{
	 "id": 1234,
	 "success": 1,
	 "data": {
			"domicilioFiscal": {
				 "codPostal": "6300",
				 "localidad": "SANTA ROSA",
				 "direccion": "TOMAS MASON 246",
				 "idProvincia": 21
			},
			"domicilioReal": {
				 "codPostal": "6300",
				 "localidad": "SANTA ROSA",
				 "direccion": "TOMAS MASON 246",
				 "idProvincia": 21
			},
			"impuestos": [
				 11
			],
			"telefono": 422993,
			"email": "treserres@pmrivas.com",
			"nombre": "RIVAS PABLO MANUEL",
			"idPersona": 20229360249,
			"domicilio": "TOMAS MASON 246",
			"localidad": "SANTA ROSA",
			"codigoPostal": "6300",
			"provincia": "LA PAMPA",
			"iiBB:": "44444/4",
			"agenteIIBB": "0",
			"situacionIVA": "RM",
			"todas": {
				 "actividad": [
						{
							 "descripcionActividad": "SERVICIOS DE ASESORAMIENTO, DIRECCI\u00d3N Y GESTI\u00d3N EMPRESARIAL N.C.P.",
							 "idActividad": 702099,
							 "nomenclador": 883,
							 "orden": 1,
							 "periodo": 201311
						}
				 ],
				 "apellido": "RIVAS",
				 "categoria": [
						{
							 "descripcionCategoria": "CATEGORIA D, VIGENTE HASTA 28\/02\/2007",
							 "estado": "BAJA DEFINITIVA",
							 "idCategoria": 40,
							 "idImpuesto": 308,
							 "periodo": 200406
						}
				 ],
				 "domicilio": [
						{
							 "codPostal": "6300",
							 "descripcionProvincia": "LA PAMPA",
							 "direccion": "TOMAS MASON 246",
							 "idProvincia": 21,
							 "localidad": "SANTA ROSA",
							 "tipoDomicilio": "FISCAL"
						},
						{
							 "codPostal": "6300",
							 "descripcionProvincia": "LA PAMPA",
							 "direccion": "TOMAS MASON 246",
							 "idProvincia": 21,
							 "localidad": "SANTA ROSA",
							 "tipoDomicilio": "LEGAL\/REAL"
						}
				 ],
				 "email": [
						{
							 "direccion": "treserres@pmrivas.com",
							 "estado": "No Confirmado por Aplicacion",
							 "tipoEmail": "COMERCIAL"
						}
				 ],
				 "estadoClave": "ACTIVO",
				 "fechaInscripcion": "1994-05-16T12:00:00-03:00",
				 "fechaNacimiento": "1972-10-01T12:00:00-03:00",
				 "idPersona": 20229360249,
				 "impuesto": [
						{
							 "descripcionImpuesto": "GANANCIAS PERSONAS FISICAS",
							 "diaPeriodo": 2,
							 "estado": "ACTIVO",
							 "ffInscripcion": "2000-10-02T12:00:00-03:00",
							 "idImpuesto": 11,
							 "periodo": 200401
						},
						{
							 "descripcionImpuesto": "APORTES SEG.SOCIAL AUTONOMOS",
							 "diaPeriodo": 30,
							 "estado": "BAJA DEFINITIVA",
							 "ffInscripcion": "1992-01-01T12:00:00-02:00",
							 "idImpuesto": 308,
							 "periodo": 200406
						}
				 ],
				 "mesCierre": 12,
				 "nombre": "PABLO MANUEL",
				 "numeroDocumento": "22936024",
				 "sexo": "MASCULINO",
				 "telefono": [
						{
							 "numero": 422993,
							 "tipoLinea": "FIJO",
							 "tipoTelefono": "COMERCIAL"
						}
				 ],
				 "tipoClave": "CUIT",
				 "tipoDocumento": "DNI",
				 "tipoPersona": "FISICA"
			}
	 }
}
```

**En caso de empresa YA CREADA**, es decir, una empresa ya dada de alta para ese mismo ente, retorna la id de la empresa ya existente y cambia su status:

```http
Status: 202 Accepted
```
```json
{
	 "id":  1234,
	 "success": 1,
	 "data": {
			"domicilioFiscal": {
				...
				}
			}
}
```

**En caso de cuit registrado pero de otro ente**, retorna status 409 Conflict, y un array con los entes ya registrados a esa empresa. 

```http
Status: 409 Conflict
```
```json
{
	 "err":  409,
	 "txerr":  "Empresa asignada a otros usuarios",
	 "entes" : ["123123","123123","33333"]
}
```
