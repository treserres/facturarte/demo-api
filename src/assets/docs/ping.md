Este método permite controlar la validez de las credenciales, y realiza un simple "ping" con su consiguiente respuesta "pong".

### Request

* La cabecera deberá incluir las credenciales UID y KEY separadas por el símbolo `:` (dos puntos) codificados en base64.  Ejemplo: UID 123, KEY: a654a654a654, el resultado sería `MTIzOmE2NTRhNjU0YTY1NAo=` 

```http
Authentication: MTIzOmE2NTRhNjU0YTY1NAo=
```

```json
{ }
```

### Response

Responde un booleano "pong".

```json
{
    "pong": true
}
```

Para errores de respuesta, consulte [Condigos de Respuesta](codigosrespuesta).